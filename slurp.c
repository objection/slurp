#define _GNU_SOURCE
#include "slurp.h"
#include <stdio.h>
#include <errno.h>
#include <stdarg.h>
#include <assert.h>
#include <stdlib.h>

char *slurp_fp (FILE *f) {
	char *r = 0;
	size_t n_buf = 0; 	// the size of the allocation in bytes
#if 0
	ssize_t read = 0;	// the number of chars read or -1 if fail
#endif
	errno = 0;
	int ret;
	if ((ret = getdelim (&r, &n_buf, '\0', f)) == -1) {
		// I don't know if this is the right way to deal with it.
		// It transpires that getdelim can return -1 _and_ have allocated the
		// memory. That's absolutely insane.
		if (r)
			free (r);
		return 0;
	}

	// No need to check read. Res will be 0.
	return r;
}

char *slurp (const char *path) {
	errno = 0;

	// If the path is "-", we read from stdin. I think that's fine.
	// Obviously this will fail if you're trying to make a file called
	// but, well, if you're doing that, just don't use this. I suppose
	// I could use a thread-local variable, or accept an escaped "-".
	// I wouldn't to add another parameter.
	FILE *f = *path == '-' && path[1] == 0
		? stdin
		: fopen (path, "re");
	if (f == 0) return 0;  	// use errno if returns 0
	char *r = slurp_fp (f);
	fclose (f);
	return r; // r might be 0.

}

char *slurp_pipe (const char *command) {
	errno = 0;
	FILE *f = popen (command, "re");
	if (!f) return 0;  	// use errno if returns 0
	char *r = slurp_fp (f);

	// This bit here is why slurp_pipe has only limited usefulness.
	// pclose returns -1 if it fails, else the result of the command,
	// which could have failed, itself. Hence, you'll never know what
	// went wrong if something did. Still, I think it's fine if
	// you think the cmd will probably succeed. Of course, we could
	// return a struct or an exit code.
	pclose (f) == 0 ?: ({
		if (r) free (r); // Free doesn't set errno, so you can use it.
		return 0;
	});
	return r; // r might be 0
}

int spurt (char *path, char *fmt) {

	va_list ap;
	char *buf = 0;

	vasprintf (&buf, fmt, ap);
	va_end (ap);
	FILE *f = fopen (path, "we");
	if (f == 0) return 1;
	fprintf (f, "%s", buf);
	fclose (f);
	return 0;
}

